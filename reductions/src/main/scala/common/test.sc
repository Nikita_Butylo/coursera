def balance(s: String): Boolean = {

  def helper(s: String, acc: Int): Boolean = {
    if (acc < 0) false
    else if (s.isEmpty) acc == 0
    else {
      if (s.head == '(')
        helper(s.tail, acc + 1)
      else if (s.head == ')')
        helper(s.tail, acc - 1)
      else helper(s.tail, acc)
    }
  }

  helper(s, 0)
}

balance("())(")