package reductions

import java.util.concurrent._
import scala.collection._
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import common._

import ParallelParenthesesBalancing._

@RunWith(classOf[JUnitRunner])
class ParallelParenthesesBalancingSuite extends FunSuite {

  test("balance should work for empty string") {
    def check(input: String, expected: Boolean) =
      assert(balance(input.toArray) == expected,
        s"balance($input) should be $expected")

    check("", true)
  }

  test("balance should work for string of length 1") {
    def check(input: String, expected: Boolean) =
      assert(balance(input.toArray) == expected,
        s"balance($input) should be $expected")

    check("(", false)
    check(")", false)
    check(".", true)
  }

  test("balance should work for string of length 2") {
    def check(input: String, expected: Boolean) =
      assert(balance(input.toArray) == expected,
        s"balance($input) should be $expected")

    check("()", true)
    check(")(", false)
    check("((", false)
    check("))", false)
    check(".)", false)
    check(".(", false)
    check("(.", false)
    check(").", false)
  }

  test("parallel balance should work for string of length 4") {
    def check(input: String, expected: Boolean) =
      assert(parBalance(input.toArray, 2) == expected,
        s"balance('$input') should be $expected")

    check("()()", true)
    check("(())", true)
    check(")(()", false)
    check("())(", false)
    check("))))", false)
    check(".)((", false)
    check(".()(", false)
    check("(.)(", false)
    check(").)(", false)
  }

  test("parallel balance should work for string of different lengths") {
    def check(input: String, expected: Boolean) =
      assert(parBalance(input.toArray, 2) == expected,
        s"balance('$input') should be $expected")

    check("()(())()", true)
    check("((()))()()", true)
    check("((())(()()))()", true)
    check(")(())()", false)
    check("())())(", false)
    check(")adf)f0a9)()))", false)
    check(".)(())(()(()((((())))9f()(", false)
    check(".()()(()())(((((", false)
    check("(.)(", false)
    check(").)(", false)
  }
}